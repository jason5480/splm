#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include "ui_window.h"
#include "model.h"

QT_BEGIN_NAMESPACE
class QMainWindow;
QT_END_NAMESPACE

class Window : public QWidget, private Ui::Window
{
Q_OBJECT

public:
    Window(QMainWindow *mw);
    Window(QString filename, QMainWindow *mw);
    Window(QString filename, QString partsname, QMainWindow *mw);
    Window(Model& model, QMainWindow *mw, bool withError = false);

    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void setPartitions(QString partsname);
    void setBounds(QString boundsname);
    void setAnchor(QString anchorname);
    void setglWidget(GLWidget *gl) {glWidget = gl;}
    Model* getModel() {return glWidget->getModel();}
    GLWidget *getglWidget() {return glWidget;}

public slots:
    void dockUndock();
    void rotateY();

private slots:
    void on_wireframeCheckBox_stateChanged(int state);
    void on_partitionsCheckBox_stateChanged(int state);
    void on_anchorsCheckBox_stateChanged(int state);
    void on_glWidget_aboutToCompose();
 
private:
    QString filename;
    QMainWindow *mainWindow;
};

#endif
