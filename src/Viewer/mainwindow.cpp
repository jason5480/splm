//--------------------------------------------------------------------------------//
//                        VVR LAB 2015                                            //
//   Sparse Processing of Laplacian meshes for efficient Geometry Compression     //
//Authors: Aris S. Lalos, Iason Nikolas, Viktor Kyriazakos, Konstantinos Moustakas//
//code written by A. Lalos, I. Nikolas, V. Kyriazakos                             //
//--------------------------------------------------------------------------------//
#include "mainwindow.h"
#include "window.h"
#include <QMenuBar>
#include <QMenu>
#include <QMessageBox>
#include <QFileDialog>
#include <QDialog>
#include "glwidget.h"
#include "model.h"
#include "encodingwindow.h"
#include <windows.h>
#include <fstream>

MainWindow::MainWindow() : animate(false),
                           angleY(0)
{
    setupUi(this);

    m_timer.setInterval(100);

    Ui::MainWindow::centralWidget->deleteLater();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    Window* win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    if (win) win->keyPressEvent(event);
}

void MainWindow::on_actionAddNew_triggered()
{
    Window* win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    if (win)
    {
        QMessageBox::information(0, tr("Undock previous window"), tr("Previous window will be undocked!"));
        win->dockUndock();
    }
    QString modelsdir = QCoreApplication::applicationDirPath() + "/../../resources/Models/";
    //QString modelsdir("D:/Dropbox/3D Compression/Airway Models");
    //QString modelsdir("D:/Desktop/paper");
    QString modelFilename = QFileDialog::getOpenFileName(this, tr("Select .obj file"), modelsdir, tr("OBJ Files (*.obj)"));
    if (!modelFilename.isEmpty())
    {
        Window* win = new Window(modelFilename, this);
        connect(&m_timer, &QTimer::timeout, this, &MainWindow::changeAngle);
        connect(this, &MainWindow::angleChanged, win->getglWidget(), &GLWidget::setYRotation);
        QMainWindow::setCentralWidget(win);
    }
}

void MainWindow::on_actionPartitions_triggered()
{
    Window* win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    if (win)
    {
        QString partsdir = QCoreApplication::applicationDirPath() + "/../../resources/Partitions/";
        //QString partsdir("D:/Dropbox/3D Compression/Airway Models");
        QString partsFilename = QFileDialog::getOpenFileName(this, tr("Select parts file"), partsdir);
        if (!partsFilename.isEmpty())
        {
            /*
            Model model = *(win->getModel());
            model.loadPartitions(partsFilename.toStdString());
            win = new Window(model, this, false);
            //*/
            Model* model = win->getModel();
            model->loadPartitions(partsFilename.toStdString());
            model->populateColors();
            win->getglWidget()->colorsChanged();
            connect(&m_timer, &QTimer::timeout, this, &MainWindow::changeAngle);
            connect(this, &MainWindow::angleChanged, win->getglWidget(), &GLWidget::setYRotation);
            QMainWindow::setCentralWidget(win);
        }
        emit partitionsLoaded(true);
    }
    else
    {
        QMessageBox::information(0, tr("Cannot load Anchor Points"), tr("Add a new window first or dock an existing one!"));
        emit partitionsLoaded(false);
    }
}

void MainWindow::on_actionBounds_triggered()
{
    Window* win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    if (win)
    {
        QString boundsdir = QCoreApplication::applicationDirPath() + "/../../resources/Bounds/";
        //QString boundsdir("D:/Dropbox/3D Compression/video objs");
        //QString boundsdir("D:/Dropbox/3D Compression/Airway Models");
        QString boundsFilename = QFileDialog::getOpenFileName(this, tr("Select bounds file"), boundsdir, tr("Bounds Files (*.bnds)"));
        if (!boundsFilename.isEmpty())
        {
            /*
            Model model = *(win->getModel());
            model.loadBounds(boundsFilename.toStdString());
            win = new Window(model, this, false);
            //*/
            //*
            Model* model = win->getModel();
            model->loadBounds(boundsFilename.toStdString());
            model->populateColors();
            win->getglWidget()->colorsChanged();
            //*/
            connect(&m_timer, &QTimer::timeout, this, &MainWindow::changeAngle);
            connect(this, &MainWindow::angleChanged, win->getglWidget(), &GLWidget::setYRotation);
            QMainWindow::setCentralWidget(win);
        }
        emit boundsLoaded(true);
    }
    else
    {
        QMessageBox::information(0, tr("Cannot load Bounds"), tr("Add a new window first or dock an existing one!"));
        emit boundsLoaded(false);
    }
}

void MainWindow::on_actionAnchorPoints_triggered()
{
    Window* win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    if (win)
    {
        QString anchordir = QCoreApplication::applicationDirPath() + "/../../resources/Anchor/";
        QString anchorFilename = QFileDialog::getOpenFileName(this, tr("Select .anc file"), anchordir, tr("Anchor Files (*.anc)"));
        if (!anchorFilename.isEmpty())
        {
            Model model = *(win->getModel());
            model.loadAnchorPoints(anchorFilename.toStdString());
            win = new Window(model, this, false);
            connect(&m_timer, &QTimer::timeout, this, &MainWindow::changeAngle);
            connect(this, &MainWindow::angleChanged, win->getglWidget(), &GLWidget::setYRotation);
            QMainWindow::setCentralWidget(win);
        }
        emit anchorsLoaded(true);
    }
    else
    {
        QMessageBox::information(0, tr("Cannot load Anchor Points"), tr("Add a new window first or dock an existing one!"));
        emit anchorsLoaded(false);
    }
}

void MainWindow::on_actionShowError_triggered()
{
    Window* win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    if (win)
    {
        QString errordir = QCoreApplication::applicationDirPath() + "/../../resources/Error/";
        QString errorFilename = QFileDialog::getOpenFileName(this, tr("Select .err file"), errordir, tr("Error Files (*.err)"));
        if (!errorFilename.isEmpty())
        {
            //Open Error file and write values into model class members
            std::ifstream infile(errorFilename.toStdString());

            double min, max;
            infile >> min >> max;

            //qDebug()<<"Min: "<<min<<"Max: "<<max;
            vector<double> Error;
            double tmp;
            //int j=0;

            while (infile >> tmp)
            {
                Error.push_back(tmp);
                //qDebug() << Error[j]<<endl;
                //j+=1;
            }

            GLWidget* widget = win->getglWidget();
            win->getModel()->setErrorMax(max);
            win->getModel()->setErrorMin(min);
            if (Error.size() != win->getModel()->m_vertices.size())
            {
                QMessageBox msgBox;
                msgBox.setText("Error count doesn't match vertex count. Probably wrong model loaded!");
                msgBox.exec();
            }
            else
            {
                // TODO: FIND WHY MODEL IS EMPTY
                Model model = *(win->getModel());
                model.m_colorPerVertex.clear();

                // Convert Error to HSV
                float R, G, B, H, S = 1.0f, V = 1.0f;
                float var_h, var_1, var_2, var_3, var_r, var_g, var_b;
                int var_i;
                for (int i = 0; i < (int)win->getModel()->m_vertices.size(); i++)
                {
                    // Scale H -> 0-1
                    H = (Error[i] - min) / (max - min);
                    if (S == 0)                       // HSV from 0 to 1
                    {
                        R = V;
                        G = V;
                        B = V;
                    }
                    else
                    {
                        var_h = H * 6;
                        if (var_h == 6) var_h = 0;
                        var_i = int(var_h);
                        var_1 = V * (1 - S);
                        var_2 = V * (1 - S * (var_h - var_i));
                        var_3 = V * (1 - S * (1 - (var_h - var_i)));

                        if (var_i == 0) { var_r = V; var_g = var_3; var_b = var_1; }
                        else if (var_i == 1) { var_r = var_2; var_g = V; var_b = var_1; }
                        else if (var_i == 2) { var_r = var_1; var_g = V; var_b = var_3; }
                        else if (var_i == 3) { var_r = var_1; var_g = var_2; var_b = V; }
                        else if (var_i == 4) { var_r = var_3; var_g = var_1; var_b = V; }
                        else                   { var_r = V; var_g = var_1; var_b = var_2; }

                        R = var_r;
                        G = var_g;
                        B = var_b;
                        model.m_colorPerVertex.push_back(Color3(R, G, B));
                    }
                }
                model.setError(Error);
                win = new Window(model, this, true);
                QMainWindow::setCentralWidget(win);
            }
        }
        emit errorLoaded(true);
    }
    else
    {
        QMessageBox::information(0, tr("Cannot load Error File"), tr("Add a new window first or dock an existing one!"));
        emit errorLoaded(false);
    }
}

void MainWindow::on_actionSaveAs_triggered()
{
    qDebug() << "onSaveAs";
    Window* win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    if (win)
    {
        QFileDialog dialog(this);
        QString modelsdir = QCoreApplication::applicationDirPath() + "/../../resources/Models/";
        QString filename = dialog.getSaveFileName(this, tr("Give .obj filename"), modelsdir, tr("OBJ Files (*.obj)"));
        if (!filename.isEmpty()) win->getModel()->saveOBJ(filename.toStdString());
    }
}

void MainWindow::on_actionGraph_triggered()
{
    qDebug() << "onGraph";
    Window* win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    if (win)
    {
        QFileDialog dialog(this);
        QString graphsdir = QCoreApplication::applicationDirPath() + "/../../resources/Graphs/";
        QString filename = dialog.getSaveFileName(this, tr("Give .graph filename"), graphsdir, tr("GRAPH Files (*.graph)"));
        if (!filename.isEmpty()) win->getModel()->saveGraph(filename.toStdString());
    }
}

void MainWindow::on_actionMeTiS_triggered()
{
    Window *win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    QString filename;
    QString partsFilename;

    if (win)
    {
        Model cur_Obj = *win->getModel();
        QString objpath = QString::fromStdString(cur_Obj.m_objFilename);
        qDebug() << "The filename is: " << objpath;
        QRegExp rx("(\\.|\\/)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
        QStringList query = objpath.split(rx);
        QString name = query.value(query.length() - 2);

        qDebug() << "onMeTiS";
        //metis(2, "-ptype=rb E:\Workspace_VC\metis-5.1.0\metis-5.1.0\graphs\4elt.graph 5");

        QString Appdir = QCoreApplication::applicationDirPath();
        QRegExp rx2("vc10"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
        QStringList query2 = Appdir.split(rx2);
        QString name2 = query2.value(query2.length() - 2);
        QString exe = name2 + QString("src/Viewer/gpmetis.exe ");
        qDebug() << exe;

        filename = name2 + "resources/Graphs/" + name + ".graph";
        if (!filename.isEmpty())
        {
            //qDebug()<<QString("The partition number is") << epDialog->getgraphpartitions();

            QString par = filename + QString(" 15");
            QString cmnd = exe + par;
            qDebug() << cmnd;
            if (~system(cmnd.toStdString().c_str()))
            partsFilename = filename + QString(".part.") + QString("15");

            // int secs=0;
            //while(fopen ( partsFilename.toStdString().c_str(), "r")==NULL){
            //    Sleep(1000);
            //    qDebug()<<QString("Program is paused")<<++secs<< QString("seconds");
            //    qDebug()<<partsFilename;
            //}
            //qDebug()<<partsFilename;
        }

        win->getglWidget()->close();

        cur_Obj.loadPartitions(partsFilename.toStdString());

        Window* win2 = new Window(cur_Obj, this);
        QMainWindow::setCentralWidget(win2);
    }

    //GLWidget *glWidget2=new GLWidget(win);

    //win->getglWidget()->cleanup();

    //win->glWidget->cleanup();

    // qDebug()<< QString::fromStdString(Cur_Obj->objFilename)<< partsFilename;
    //QMainWindow::setCentralWidget(win);
    //glWidget2->setModelName(filename);
    //glWidget2->setModelParts(partsFilename);
    //glWidget2->init();
    //win->setglWidget(glWidget2);
    //win = new Window(filename, partsFilename,this);

    //    QMainWindow::setCentralWidget(win);

    //QRegExp rx("(\\ |\\,|\\.|\\:|\\t)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
    //QStringList query = filename.split(rx);
    //qDebug() << query;

    //const char* c = scmnd.c_str();
    //(void) system(c);
    //(void) system("E:\\Workspace_VC\\metis-5.1.0\\vc10\\programs\\Release\\gpmetis.exe" );
    /*
    idx_t i;
    char *curptr, *newptr;
    idx_t options[METIS_NOPTIONS];
    graph_t *graph;
    idx_t *part;
    idx_t objval;
    params_t *params;
    int status=0;

    //params = parse_cmdline(argc, argv);
    params->filename = "E:/Workspace_VC/SPLM/resources/Graphs/tyra_low.graph";
    params->nparts = 20;

    gk_startcputimer(params->iotimer);
    graph = ReadGraph(params);

    ReadTPwgts(params, graph->ncon);
    gk_stopcputimer(params->iotimer);

    // Check if the graph is contiguous
    if (params->contig && !IsConnected(graph, 0)) {
    printf("***The input graph is not contiguous.\n"
    "***The specified -contig option will be ignored.\n");
    params->contig = 0;
    }

    // Get ubvec if supplied
    if (params->ubvecstr) {
    params->ubvec = rmalloc(graph->ncon, "main");
    curptr = params->ubvecstr;
    for (i=0; i<graph->ncon; i++) {
    params->ubvec[i] = strtoreal(curptr, &newptr);
    if (curptr == newptr)
    errexit("Error parsing entry #%"PRIDX" of ubvec [%s] (possibly missing).\n",
    i, params->ubvecstr);
    curptr = newptr;
    }
    }

    // Setup iptype
    if (params->iptype == -1) {
    if (params->ptype == METIS_PTYPE_RB) {
    if (graph->ncon == 1)
    params->iptype = METIS_IPTYPE_GROW;
    else
    params->iptype = METIS_IPTYPE_RANDOM;
    }
    }

    GPPrintInfo(params, graph);

    part = imalloc(graph->nvtxs, "main: part");

    METIS_SetDefaultOptions(options);
    options[METIS_OPTION_OBJTYPE] = params->objtype;
    options[METIS_OPTION_CTYPE]   = params->ctype;
    options[METIS_OPTION_IPTYPE]  = params->iptype;
    options[METIS_OPTION_RTYPE]   = params->rtype;
    options[METIS_OPTION_NO2HOP]  = params->no2hop;
    options[METIS_OPTION_MINCONN] = params->minconn;
    options[METIS_OPTION_CONTIG]  = params->contig;
    options[METIS_OPTION_SEED]    = params->seed;
    options[METIS_OPTION_NITER]   = params->niter;
    options[METIS_OPTION_NCUTS]   = params->ncuts;
    options[METIS_OPTION_UFACTOR] = params->ufactor;
    options[METIS_OPTION_DBGLVL]  = params->dbglvl;

    gk_malloc_init();
    gk_startcputimer(params->parttimer);

    switch (params->ptype) {
    case METIS_PTYPE_RB:
    status = METIS_PartGraphRecursive( &graph->nvtxs, &graph->ncon, graph->xadj,
    graph->adjncy, graph->vwgt, graph->vsize, graph->adjwgt,
    &params->nparts, params->tpwgts, params->ubvec, options,
    &objval, part);
    break;

    case METIS_PTYPE_KWAY:
    status = METIS_PartGraphKway(&graph->nvtxs, &graph->ncon, graph->xadj,
    graph->adjncy, graph->vwgt, graph->vsize, graph->adjwgt,
    &params->nparts, params->tpwgts, params->ubvec, options,
    &objval, part);
    break;
    }

    gk_stopcputimer(params->parttimer);

    if (gk_GetCurMemoryUsed() != 0)
    printf("***It seems that Metis did not free all of its memory! Report this.\n");
    params->maxmemory = gk_GetMaxMemoryUsed();
    gk_malloc_cleanup(0);

    if (status != METIS_OK) {
    printf("\n***Metis returned with an error.\n");
    }
    else {
    if (!params->nooutput) {
    // Write the solution
    gk_startcputimer(params->iotimer);
    WritePartition(params->filename, part, graph->nvtxs, params->nparts);
    gk_stopcputimer(params->iotimer);
    }

    GPReportResults(params, graph, part, objval);
    }

    FreeGraph(&graph);
    gk_free((void **)&part, LTERM);
    gk_free((void **)&params->filename, &params->tpwgtsfile, &params->tpwgts,
    &params->ubvecstr, &params->ubvec, &params, LTERM);
    //*/
}

void MainWindow::on_actionEncoder_triggered()
{
    int tmp1;
    double tmp2;
    Model *Cur_Obj = NULL;
    Window *win = dynamic_cast<Window*>(QMainWindow::centralWidget());
    QString Vnum;
    QString Enum;

    qDebug() << "onBounds";
    EnWindow *epDialog = new EnWindow(this);

    if (win) {
        Cur_Obj = win->getModel();
        qDebug() << " Model exist" << Cur_Obj->m_vertices.size();
        Vnum.setNum(Cur_Obj->m_vertices.size());
        epDialog->setvnum(Vnum);
        qDebug() << " Model exist edges: " << Cur_Obj->m_edges;
        Enum.setNum(Cur_Obj->m_edges);
        epDialog->setenum(Enum);
    }

    if (epDialog->exec()) {
        tmp1 = epDialog->getgraphpartitions();
        qDebug() << tmp1;

        tmp2 = epDialog->getCR();
        qDebug() << tmp2;
    }
}

void MainWindow::on_actionAnimate_triggered()
{
    cout << "Animate" << endl;
    if (!animate)
    {
        m_timer.start();
        animate = true;
    }
    else
    {
        m_timer.stop();
        animate = false;
    }
}

void MainWindow::changeAngle()
{
    angleY += 15;
    emit angleChanged(angleY);
}