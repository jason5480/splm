#ifndef _Vertex_H_
#define _Vertex_H_

//--------------------------------------------------------------------------------//
//                        VVR LAB 2015                                            //
//   Sparse Processing of Laplacian meshes for efficient Geometry Compression     //                                                                                //
//Authors: Aris S. Lalos, Iason Nikolas, Viktor Kyriazakos, Konstantinos Moustakas//
//code written by A. Lalos, I. Nikolas, V. Kyriazakos                             //                                            //
//--------------------------------------------------------------------------------//

#include "vec3.h"
#include <vector>

/*******************************************************************************
  Vertex
 ******************************************************************************/

template <typename T>
class Vertex
{
public:
    // ctors
    Vertex();
    Vertex(const Vertex& other);
    Vertex(Vec3<T>& p,Vec3<T>& d,Vec3<T>& rp,Vec3<T>& rd ,int par=-1);

    ~Vertex();

    // Data members
    Vec3<T> pos;
    Vec3<T> delta;
    Vec3<T> rpos;
    Vec3<T> rdelta;

    int     part;

    std::vector<int> adjacentVerticies;
    std::vector<int> adjacentTriangles;
};

template <typename T>
inline Vertex<T>::Vertex() : pos(), part(-1), delta(), rpos(), rdelta()
{}

template <typename T>
inline Vertex<T>::Vertex(const Vertex& other) : pos(other.pos), part(other.part), delta(other.delta), rpos(other.rpos), rdelta(other.rdelta)
{}

template <typename T>
inline Vertex<T>::Vertex(Vec3<T>& p, Vec3<T>& d,Vec3<T>& rp,Vec3<T>& rd, int par) : pos(p), part(par), delta(d), rpos(rp), rdelta(rd) 
{}

template <typename T>
inline Vertex<T>::~Vertex()
{}

template <typename T>
std::ostream& operator << (std::ostream& os, const Vertex<T>& v)
{
    os << " Pos: " << v.pos <<" Delta: "<< v.delta << " Norm: " << v.norm << " Part: " << v.part << " AjTringles: ";
    for (size_t i = 0; i < adjacentTriangles.size(); i++) os << adjacentTriangles[1] << "\t";
    os << "AjVertices: ";
    for (size_t i = 0; i < adjacentVertices.size(); i++) os << adjacentVertices[1] << "\t";
    return os;
}


#endif