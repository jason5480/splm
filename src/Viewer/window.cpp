//--------------------------------------------------------------------------------//
//                        VVR LAB 2015                                            //
//   Sparse Processing of Laplacian meshes for efficient Geometry Compression     //
//Authors: Aris S. Lalos, Iason Nikolas, Viktor Kyriazakos, Konstantinos Moustakas//
//code written by A. Lalos, I. Nikolas, V. Kyriazakos                             //
//--------------------------------------------------------------------------------//
#include "glwidget.h"
#include "window.h"
#include "mainwindow.h"
#include <QSlider>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QPushButton>
#include <QDesktopWidget>
#include <QApplication>
#include <QMessageBox>

Window::Window(QMainWindow *mw) : mainWindow(mw)
{
    setupUi(this);

    mainWindow = mw;
    wireframeCheckBox->setCheckState(Qt::PartiallyChecked);

    connect(dockBtn, &QPushButton::clicked, this, &Window::dockUndock);
    connect(glWidget, &GLWidget::wireframeChanged, wireframeCheckBox, &QCheckBox::setCheckState);

    MainWindow* mainwindow = dynamic_cast<MainWindow*>(mw);
    if (mainwindow)
    {
        connect(mainwindow, &MainWindow::partitionsLoaded, partitionsCheckBox, &QCheckBox::setCheckable);
        connect(mainwindow, &MainWindow::anchorsLoaded, anchorsCheckBox, &QCheckBox::setCheckable);
    }

    setWindowTitle(tr("SPLM Viewer"));
}

Window::Window(QString filename, QMainWindow *mw)
{
    setupUi(this);

    mainWindow = mw;
    wireframeCheckBox->setCheckState(Qt::PartiallyChecked);
    
    glWidget->setModelName(filename);
    glWidget->init();

    connect(dockBtn, &QPushButton::clicked, this, &Window::dockUndock);
    connect(glWidget, &GLWidget::wireframeChanged, wireframeCheckBox, &QCheckBox::setCheckState);

    MainWindow* mainwindow = dynamic_cast<MainWindow*>(mw);
    if (mainwindow)
    {
        connect(mainwindow, &MainWindow::partitionsLoaded, partitionsCheckBox, &QCheckBox::setCheckable);
        connect(mainwindow, &MainWindow::anchorsLoaded, anchorsCheckBox, &QCheckBox::setCheckable);
    }

    setWindowTitle(filename);
}

Window::Window(QString filename, QString partsname, QMainWindow *mw)
  : mainWindow(mw)
{
    setupUi(this);

    glWidget->setModelName(filename);
    glWidget->setModelParts(partsname);
    glWidget->init();
  
    connect(dockBtn, &QPushButton::clicked, this, &Window::dockUndock);
    connect(glWidget, &GLWidget::wireframeChanged, wireframeCheckBox, &QCheckBox::setCheckState);

    MainWindow* mainwindow = dynamic_cast<MainWindow*>(mw);
    if (mainwindow)
    {
        connect(mainwindow, &MainWindow::partitionsLoaded, partitionsCheckBox, &QCheckBox::setCheckable);
        connect(mainwindow, &MainWindow::anchorsLoaded, anchorsCheckBox, &QCheckBox::setCheckable);
    }

    setWindowTitle(filename);
}

Window::Window(Model& model, QMainWindow *mw, bool withError) : mainWindow(mw)
{
    setupUi(this);

    mainWindow = mw;
    wireframeCheckBox->setCheckState(Qt::PartiallyChecked);

    glWidget->setModel(model);
    glWidget->init(withError);
    
    connect(dockBtn, &QPushButton::clicked, this, &Window::dockUndock);
    connect(glWidget, &GLWidget::wireframeChanged, wireframeCheckBox, &QCheckBox::setCheckState);

    MainWindow* mainwindow = dynamic_cast<MainWindow*>(mw);
    if (mainwindow)
    {
        connect(mainwindow, &MainWindow::partitionsLoaded, partitionsCheckBox, &QCheckBox::setCheckable);
        connect(mainwindow, &MainWindow::anchorsLoaded, anchorsCheckBox, &QCheckBox::setCheckable);
    }
    if (model.m_objFilename.empty())
        setWindowTitle(QString("Error Visualization"));
    else
        setWindowTitle(QString::fromUtf8(model.m_objFilename.c_str()));
}

void Window::keyPressEvent(QKeyEvent *e)
{
    QWidget::keyPressEvent(e);
    if (e->key() == Qt::Key_Escape)
        close();
    else
        QWidget::keyPressEvent(e);
}

void Window::keyReleaseEvent(QKeyEvent *e)
{
    QWidget::keyReleaseEvent(e);
}

void Window::setPartitions(QString partsname)
{

}

void Window::setBounds(QString boundsname)
{
    cout << "Window load bounds" << endl;
    glWidget->setModelBounds(boundsname);
    glWidget->init();
}

void Window::setAnchor(QString anchorname)
{

}

void Window::dockUndock()
{
    if (parent())
    {
        MainWindow* mainwindow = dynamic_cast<MainWindow*>(parent());
        if (mainwindow)
        {
            disconnect(mainwindow, &MainWindow::partitionsLoaded, partitionsCheckBox, &QCheckBox::setCheckable);
            disconnect(mainwindow, &MainWindow::anchorsLoaded, anchorsCheckBox, &QCheckBox::setCheckable);
        }

        setParent(0);
        setAttribute(Qt::WA_DeleteOnClose);
        move(QApplication::desktop()->width() / 2 - width() / 2,
             QApplication::desktop()->height() / 2 - height() / 2);
        dockBtn->setText(tr("Dock"));
        resize(945, 998); // 2x1
        //resize(625, 998); // 3x1
        //resize(945, 485); // 2x2
        show();
    }
    else
    {
        MainWindow* mainwindow = dynamic_cast<MainWindow*>(mainWindow);
        if (mainwindow)
        {
            if (!mainWindow->centralWidget())
            {
                if (mainWindow->isVisible())
                {
                    setAttribute(Qt::WA_DeleteOnClose, false);
                    mainWindow->setCentralWidget(this);
                    dockBtn->setText(tr("Undock"));
                    connect(mainwindow, &MainWindow::partitionsLoaded, partitionsCheckBox, &QCheckBox::setCheckable);
                    connect(mainwindow, &MainWindow::anchorsLoaded, anchorsCheckBox, &QCheckBox::setCheckable);
                }
                else
                {
                    QMessageBox::information(0, tr("Cannot dock"), tr("Main window already closed"));
                }
            }
            else
            {
                QMessageBox::information(0, tr("Cannot dock"), tr("Main window already occupied"));
            }
        }
        else cout << "MainWindow not initiated" << endl;
    }
}

void Window::rotateY()
{

}

void Window::on_wireframeCheckBox_stateChanged(int state)
{
    glWidget->m_wireframe = state;
    glWidget->update();
}

void Window::on_partitionsCheckBox_stateChanged(int state)
{
    glWidget->m_partitions = state;
    glWidget->update();
}

void Window::on_anchorsCheckBox_stateChanged(int state)
{
    glWidget->m_anchors = state;
    glWidget->update();
}

void Window::on_glWidget_aboutToCompose()
{}