#version 330
attribute vec4 vertex;
attribute vec3 normal;
attribute vec3 color;
varying vec3 vert;
varying vec3 vertNormal;
varying vec3 col;
uniform mat4 projMatrix;
uniform mat4 mvMatrix;
uniform mat3 normalMatrix;
uniform float isBlack;
void main()
{
    vert = vertex.xyz;
    vertNormal = normalMatrix * normal;
    if (isBlack > 0.5 )
	{
		col = vec3(0.0, 0.0, 0.0);
		vec4 v = mvMatrix * vertex;
		v.xyz *= 0.99;
		gl_Position = projMatrix * v;
	}
	else
	{
		col = color;
		gl_Position = projMatrix * mvMatrix * vertex;
	}
};