#version 330

// Interpolated values from the vertex shaders
in vec3 col;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;

// Ouput data
out vec3 color;

// Values that stay constant for the whole mesh.
uniform vec3 lightPosition_worldspace;
uniform vec3 lightColor;
uniform float lightPower;

void main()
{	
    // Material properties
    vec3 MaterialDiffuseColor = col;
    vec3 MaterialAmbientColor = vec3(0.1, 0.1, 0.1) * MaterialDiffuseColor;
    vec3 MaterialSpecularColor = vec3(0.3, 0.3, 0.3);
    
    // Distance to the light
    float distance = length( lightPosition_worldspace - Position_worldspace );
    
    // Normal of the computed fragment, in camera space
    vec3 n = normalize( Normal_cameraspace );
    
    // Direction of the light (from the fragment to the light)
    vec3 l = normalize( LightDirection_cameraspace );
    
    // Cosine of the angle between the normal and the light direction, 
    // clamped above 0
    //  - light is at the vertical of the triangle -> 1
    //  - light is perpendicular to the triangle -> 0
    //  - light is behind the triangle -> 0
    float cosTheta = clamp( dot( n,l ), 0,1 );
    
    // Eye vector (towards the camera)
    vec3 E = normalize(EyeDirection_cameraspace);
    
    // Direction in which the triangle reflects the light
    vec3 R = reflect(-l,n);
    
    // Cosine of the angle between the Eye vector and the Reflect vector,
    // clamped to 0
    //  - Looking into the reflection -> 1
    //  - Looking elsewhere -> < 1
    float cosAlpha = clamp( dot( E,R ), 0,1 );
    
    color = 
        // Ambient : simulates indirect lighting
        MaterialAmbientColor +
        // Diffuse : "color" of the object
        MaterialDiffuseColor * lightColor * lightPower * cosTheta/ (distance*distance) +
        // Specular : reflective highlight, like a mirror
        MaterialSpecularColor * lightColor * lightPower * pow(cosAlpha,5) / (distance*distance);
    
    //vec3 L = normalize(lightPosition_worldspace - Position_worldspace);
    //float NL = max(dot(n, L), 0.0);
    //color = clamp(col * 0.2 + col * 0.8 * NL, 0.0, 1.0);
}