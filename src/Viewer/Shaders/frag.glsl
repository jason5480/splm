#version 330
varying highp vec3 vert;
varying highp vec3 vertNormal;
varying highp vec3 col;
uniform highp vec3 lightPos;
void main()
{
    highp vec3 L = normalize(lightPos - vert);
    highp float NL = max(dot(normalize(vertNormal), L), 0.0);
    highp vec3 fincol = clamp(col * 0.2 + col * 0.8 * NL, 0.0, 1.0);
    gl_FragColor = vec4(fincol, 1.0);
};