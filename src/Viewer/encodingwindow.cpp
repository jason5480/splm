#include "mainwindow.h"
#include "window.h"
#include <QMenuBar>
#include <QMenu>
#include <QMessageBox>
#include <QFileDialog>
#include <QDialog>
#include "glwidget.h"
#include "model.h"
#include "encodingwindow.h"
#include <fstream>

QT_BEGIN_NAMESPACE
class QMainWindow;
QT_END_NAMESPACE

EnWindow::EnWindow(QMainWindow *mw) : mainWindow(mw)
{
    setupUi(this);

    connect(actionMeTis, SIGNAL(clicked()), this, SLOT(onMeTiS2()));
}

void EnWindow::onMeTiS2()
{
  Model *Cur_Obj = NULL;
  Window *win = dynamic_cast<Window*>(mainWindow->centralWidget());
  QString filename;
  QString partsFilename;

  qDebug() << "Btn clicked";

  if (win) {
    Cur_Obj = win->getModel();
    QString objpath = QString::fromStdString(Cur_Obj->m_objFilename);
    qDebug() << "The filename is: " << objpath;
    QRegExp rx("(\\.|\\/)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
    QStringList query = objpath.split(rx);
    QString name = query.value(query.length() - 2);

    qDebug() << "onMeTiS";
    //metis(2, "-ptype=rb E:\Workspace_VC\metis-5.1.0\metis-5.1.0\graphs\4elt.graph 5");

    QString Appdir = QCoreApplication::applicationDirPath();
    QRegExp rx2("vc10"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
    QStringList query2 = Appdir.split(rx2);
    QString name2 = query2.value(query2.length() - 2);
    QString exe = name2 + QString("src/Viewer/gpmetis.exe ");
    qDebug() << exe;

    filename = name2 + "resources/Graphs/" + name + ".graph";
    if (!filename.isEmpty()) {
      QString nparts;
      nparts.setNum(getgraphpartitions());
      QString par = filename + QString(" ") + nparts;
      QString cmnd = exe + par;
      qDebug() << cmnd;
      if (~system(cmnd.toStdString().c_str()))
        partsFilename = filename + QString(".part.") + nparts;
    }

    win->getglWidget()->close();

    Window* win2;
    win2 = new Window(objpath, partsFilename, mainWindow);
    mainWindow->setCentralWidget(win2);

    // Evaluate delta Coordinates for each Model
    win2->getModel()->evaluateDeltaCoordinates(getgraphpartitions());

    // Encode delta Coordinates based on given CR
    win2->getModel()->compressDelta(getCR(), getgraphpartitions());

    //win2->getModel()->DecodeSubMesh(getCR()); // It has been inlcuded in Surface Reconstruction

    //Perform Decompression and Surface Reconstruction
    win2->getModel()->surfaceReconstruction_new(getCR());
    cout << "Number of Anchors: " << getAnchors() << endl;

    cout << "Error: " << win2->getModel()->m_visualErr << " dB" << endl;

    QString res = QString::number(win2->getModel()->m_visualErr) + QString(" dB");

    this->seterror(res);

    mainWindow->setCentralWidget(win2);
  }
}