//--------------------------------------------------------------------------------//
//                        VVR LAB 2015                                            //
//   Sparse Processing of Laplacian meshes for efficient Geometry Compression     //
//Authors: Aris S. Lalos, Iason Nikolas, Viktor Kyriazakos, Konstantinos Moustakas//
//code written by A. Lalos, I. Nikolas, V. Kyriazakos                             //
//--------------------------------------------------------------------------------//
#include "glwidget.h"
#include "window.h"
#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QCoreApplication>
#include <math.h>

#define PROGRAM_VERTEX_ATTRIBUTE 0
#define PROGRAM_NORMAL_ATTRIBUTE 1
#define PROGRAM_COLOR_ATTRIBUTE 2

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent),
                                      m_xRot(0),
                                      m_yRot(0),
                                      m_zRot(0),
                                      m_dx(0),
                                      m_dy(0),
                                      m_dz(0),
                                      m_program(0),
                                      m_model(),
                                      m_cDist(170),
                                      m_zoomStep(5),
                                      m_wireframe(0),
                                      m_transparent(false)

{
    setFocusPolicy(Qt::WheelFocus);

    if (m_transparent) setAttribute(Qt::WA_TranslucentBackground);
}

GLWidget::GLWidget(std::string filename, QWidget *parent) : QOpenGLWidget(parent),
                                                            m_xRot(0),
                                                            m_yRot(0),
                                                            m_zRot(0),
                                                            m_dx(0),
                                                            m_dy(0),
                                                            m_dz(0),
                                                            m_program(0),
                                                            m_cDist(170),
                                                            m_zoomStep(5),
                                                            m_wireframe(0),
                                                            m_model(filename),
                                                            m_transparent(false)
{
    setFocusPolicy(Qt::WheelFocus);

    if (m_transparent) setAttribute(Qt::WA_TranslucentBackground);
}

GLWidget::~GLWidget()
{
    cleanup();
}

void GLWidget::setModelName(QString filename)
{
    m_model.m_objFilename = filename.toStdString();
}

void GLWidget::setModelParts(QString filename)
{
    m_model.m_partsFilename = filename.toStdString();
}

void GLWidget::setModelBounds(QString filename)
{
    cout << "glwidget set Bounds" << endl;
    m_model.m_boundsFilename = filename.toStdString();
    m_model.loadBounds();
    m_model.populateData();
    m_model.populateData();
}

void GLWidget::init(bool withColorPerVertex)
{
    if (m_model.m_vertices.empty())
    {
        if (!m_model.m_objFilename.empty()) m_model.loadOBJ();
        if (!m_model.m_partsFilename.empty()) m_model.loadPartitions();
        if (!m_model.m_boundsFilename.empty()) m_model.loadBounds();
    }
    if (withColorPerVertex)
    {
        cout << "Populate with per vertex color" << endl;
        m_model.populateDataWithError();
    }
    else
    {
        cout << "Populate without per vertex color" << endl;
        m_model.populateData();
        m_model.populateColors();
    }
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(800, 600);
}

static void qNormalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

void GLWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_xRot)
    {
        m_xRot = angle;
        update();
    }
}

void GLWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_yRot)
    {
        m_yRot = angle;
        update();
    }
}

void GLWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_zRot) {
        m_zRot = angle;
        update();
    }
}

void GLWidget::cleanup()
{
    makeCurrent();
    m_modelVbo.destroy();
    m_colorVbo.destroy();
    delete m_program;
    m_program = 0;
    doneCurrent();
}

void GLWidget::initializeGL()
{
    // In this example the widget's corresponding top-level window can change
    // several times during the widget's lifetime. Whenever this happens, the
    // QOpenGLWidget's associated context is destroyed and a new one is created.
    // Therefore we have to be prepared to clean up the resources on the
    // aboutToBeDestroyed() signal, instead of the destructor. The emission of
    // the signal will be followed by an invocation of initializeGL() where we
    // can recreate all resources.
    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &GLWidget::cleanup);
    
    initializeOpenGLFunctions();
    
    // White backround
    glClearColor(1, 1, 1, m_transparent ? 0 : 1);

    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);
    
    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE);

    glLineWidth(1);

    //QString vertPath = QCoreApplication::applicationDirPath() + "/../../src/Viewer/Shaders/vert.glsl";
    QString vertCorePath = QCoreApplication::applicationDirPath() + "/../../src/Viewer/Shaders/vertCore.glsl";
    //QString fragPath = QCoreApplication::applicationDirPath() + "/../../src/Viewer/Shaders/frag.glsl";
    QString fragCorePath = QCoreApplication::applicationDirPath() + "/../../src/Viewer/Shaders/fragCore.glsl";

    m_program = new QOpenGLShaderProgram;
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertCorePath);
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragCorePath);
    m_program->bindAttributeLocation("vertexPosition_modelspace", PROGRAM_VERTEX_ATTRIBUTE);
    m_program->bindAttributeLocation("vertexNormal_modelspace",   PROGRAM_NORMAL_ATTRIBUTE);
    m_program->bindAttributeLocation("vertexColor",               PROGRAM_COLOR_ATTRIBUTE);
    m_program->link();

    m_program->bind();
    m_projMatrixLoc = m_program->uniformLocation("projMatrix");
    m_mvMatrixLoc = m_program->uniformLocation("mvMatrix");
    m_viewMatrixLoc = m_program->uniformLocation("viewMatrix");
    m_modelMatrixLoc = m_program->uniformLocation("modelMatrix");
    m_normalMatrixLoc = m_program->uniformLocation("normalMatrix");
    m_lightPosCamLoc = m_program->uniformLocation("lightPosition_cameraspace");
    m_lightPosWorLoc = m_program->uniformLocation("lightPosition_worldspace");
    m_lightColLoc = m_program->uniformLocation("lightColor");
    m_lightPowLoc = m_program->uniformLocation("lightPower");
    m_isBlackLoc = m_program->uniformLocation("isBlack");

    // Create a vertex array object. In OpenGL ES 2.0 and OpenGL 2.x
    // implementations this is optional and support may not be present
    // at all. Nonetheless the below code works in all cases and makes
    // sure there is a VAO when one is needed.
    m_vao.create();
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);

    // Setup our vertex buffer object.
    m_modelVbo.create();
    m_modelVbo.bind();
    m_modelVbo.map(QOpenGLBuffer::ReadOnly);
    m_modelVbo.allocate(m_model.constData(), m_model.count() * sizeof(GLfloat));
    m_modelVbo.release();

    m_colorVbo.create();
    m_colorVbo.bind();
    m_modelVbo.map(QOpenGLBuffer::ReadWrite);
    m_colorVbo.allocate(m_model.constColors(), m_model.countColors() * sizeof(GLfloat));
    m_colorVbo.release();

    // Store the vertex attribute bindings for the program.
    setupVertexAttribs();

    // Our camera changes in this example.
    m_camera.setToIdentity();
    m_camera.translate(0, 0, -m_cDist);

    // Light position is fixed.
    m_program->setUniformValue(m_lightPosCamLoc, QVector3D(0, 0, 0));
    m_program->setUniformValue(m_lightPosWorLoc, QVector3D(0, 0, -m_cDist));
    m_program->setUniformValue(m_lightColLoc, QVector3D(1, 1, 1));
    m_program->setUniformValue(m_lightPowLoc, GLfloat(30000));
    m_program->setUniformValue(m_isBlackLoc, 0);

    m_program->release();
}

void GLWidget::setupVertexAttribs()
{
    m_modelVbo.bind();
    glEnableVertexAttribArray(PROGRAM_VERTEX_ATTRIBUTE);
    glEnableVertexAttribArray(PROGRAM_NORMAL_ATTRIBUTE);
    glEnableVertexAttribArray(PROGRAM_COLOR_ATTRIBUTE);
    glVertexAttribPointer(PROGRAM_VERTEX_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);
    glVertexAttribPointer(PROGRAM_NORMAL_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), reinterpret_cast<void *>(3 * sizeof(GLfloat)));
    m_modelVbo.release();
    m_colorVbo.bind();
    glVertexAttribPointer(PROGRAM_COLOR_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
    m_colorVbo.release();
}

void GLWidget::colorsChanged()
{
    cout << "Colors changed" << endl;
    m_program->bind();
    m_colorVbo.bind();
    m_colorVbo.write(0, m_model.constColors(), m_model.countColors() * sizeof(GLfloat));
    m_colorVbo.release();
    m_program->release();
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_world.setToIdentity();
    m_world.translate(m_dx, m_dy, m_dz);
    m_world.rotate(m_xRot / 16.0f, 1, 0, 0);
    m_world.rotate(m_yRot / 16.0f, 0, 1, 0);
    m_world.rotate(m_zRot / 16.0f, 0, 0, 1);

    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);
    m_program->bind();
    m_program->setUniformValue(m_projMatrixLoc, m_proj);
    m_program->setUniformValue(m_mvMatrixLoc, m_camera * m_world);
    m_program->setUniformValue(m_viewMatrixLoc, m_camera);
    m_program->setUniformValue(m_modelMatrixLoc, m_world);
    QMatrix3x3 normalMatrix = m_world.normalMatrix();
    m_program->setUniformValue(m_normalMatrixLoc, normalMatrix);

    if (m_wireframe == Qt::Unchecked)
    {
        m_program->setUniformValue(m_isBlackLoc, (GLfloat)0.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glDrawArrays(GL_TRIANGLES, 0, m_model.vertexCount());
    }
    else if (m_wireframe == Qt::PartiallyChecked)
    {
        m_program->setUniformValue(m_isBlackLoc, (GLfloat)0.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glDrawArrays(GL_TRIANGLES, 0, m_model.vertexCount());
        m_program->setUniformValue(m_isBlackLoc, (GLfloat)1.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDrawArrays(GL_TRIANGLES, 0, m_model.vertexCount());
    }
    else if (m_wireframe == Qt::Checked)
    {
        m_program->setUniformValue(m_isBlackLoc, (GLfloat)0.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDrawArrays(GL_TRIANGLES, 0, m_model.vertexCount());
    }

    m_program->release();
}

void GLWidget::resizeGL(int w, int h)
{
    m_proj.setToIdentity();
    m_proj.perspective(45.0f, GLfloat(w) / h, 0.01f, 1000.0f);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    m_lastPos = event->pos();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - m_lastPos.x();
    int dy = event->y() - m_lastPos.y();
    
    int modif = mkModif(event);

    if (modif)
    {
        m_dx += dx;
        m_dy -= dy;
        m_dz += 0;
        update();
    }
    else
    {
        if (event->buttons() & Qt::LeftButton)
        {
            setXRotation(m_xRot + 8 * dy);
            setYRotation(m_yRot + 8 * dx);
        }
        else if (event->buttons() & Qt::RightButton)
        {
            setXRotation(m_xRot + 8 * dy);
            setZRotation(m_zRot + 8 * dx);
        }
    }
    m_lastPos = event->pos();
}

void GLWidget::wheelEvent(QWheelEvent *event)
{
    m_cDist -= m_zoomStep * (event->delta() / 120);
    if (m_cDist < 10) m_cDist = 10;
    if (m_cDist > 500) m_cDist = 500;

    m_camera.setToIdentity();
    m_camera.translate(0.0, 0.0, -m_cDist);

    update();
}

void GLWidget::keyPressEvent(QKeyEvent *event)
{
    int modif = mkModif(event);
    QString c = event->text();
    unsigned char key = c.toLatin1()[0];
    //*
    switch (isprint(key) ? tolower(key) : key)
    {
        case 'a':
            qDebug() << "a pressed";
            break;
        case 'w':
            m_wireframe = ++m_wireframe%3;
            emit wireframeChanged((Qt::CheckState) m_wireframe);
            update();
            break;
        case 's':
            m_wireframe = --m_wireframe;
            if (m_wireframe < 0) m_wireframe = 2;
            emit wireframeChanged((Qt::CheckState) m_wireframe);
            update();
            break;
        case 'n':
            cout << "n pressed" << endl;
            break;
        case 'b':
            cout << "b pressed" << endl;
            break;
    }
    //if (event->key() == Qt::Key_Escape);
    //*/
}

void GLWidget::keyReleaseEvent(QKeyEvent *event)
{}

int GLWidget::mkModif(QInputEvent *event)
{
    int ctrl = event->modifiers() & Qt::ControlModifier ? 1 : 0;
    int shift = event->modifiers() & Qt::ShiftModifier ? 1 : 0;
    int alt = event->modifiers() & Qt::AltModifier ? 1 : 0;
    int modif = (ctrl << 0) | (shift << 1) | (alt << 2);
    return modif;
}