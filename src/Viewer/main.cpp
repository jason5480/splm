//--------------------------------------------------------------------------------//
//                        VVR LAB 2015                                            //
//   Sparse Processing of Laplacian meshes for efficient Geometry Compression     //
//Authors: Aris S. Lalos, Iason Nikolas, Viktor Kyriazakos, Konstantinos Moustakas//
//code written by A. Lalos, I. Nikolas, V. Kyriazakos                             //
//--------------------------------------------------------------------------------//
#include <QApplication>
#include <QDesktopWidget>
#include <QSurfaceFormat>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QSurfaceFormat fmt;
    fmt.setDepthBufferSize(24);
    fmt.setSamples(4);
    fmt.setVersion(3, 2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(fmt);

    MainWindow mainWindow;
    mainWindow.resize(mainWindow.sizeHint());
    int desktopArea = QApplication::desktop()->width() *
                     QApplication::desktop()->height();
    int widgetArea = mainWindow.width() * mainWindow.height();
    if (((float)widgetArea / (float)desktopArea) < 0.75f)
        mainWindow.show();
    else
        mainWindow.showMaximized();
    return app.exec();
}
