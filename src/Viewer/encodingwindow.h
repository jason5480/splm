#ifndef ENWINDOW_H
#define ENWINDOW_H

#include <QDialog>
#include <QWidget>
#include <QString>
#include "ui_window.h"
#include "model.h"
#include "ui_EncodingParameters.h"

QT_BEGIN_NAMESPACE
class QMainWindow;
QT_END_NAMESPACE

class EnWindow : public QDialog, private Ui::EncodingParamsDialog
{
Q_OBJECT

public:
    EnWindow(QMainWindow *mw);
    //~EnWindow(){}

    int getgraphpartitions(){return Partition_Num->value();}
    int getAnchors(){return nAnchors->value();}
    double getCR(){return CR_Num->value(); }
    void setvnum(QString Input){vertices_num->setText(Input);}
    void setenum(QString Input){edges_num->setText(Input);}
    void seterror(QString Input){VNMSE->setText(Input);}
    
public slots:
	void onMeTiS2();

private:
    QMainWindow *mainWindow;
};

#endif
