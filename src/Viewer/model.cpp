//--------------------------------------------------------------------------------//
//                        VVR LAB 2015                                            //
//   Sparse Processing of Laplacian meshes for efficient Geometry Compression     //
//Authors: Aris S. Lalos, Iason Nikolas, Viktor Kyriazakos, Konstantinos Moustakas//
//code written by A. Lalos, I. Nikolas, V. Kyriazakos                             //
//--------------------------------------------------------------------------------//

#include "model.h"
#include "triangle.h"
#include "vertex.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <random>
#include <set>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/Cholesky>
#include <Eigen/SVD>
#include <conio.h>
#include <math.h>
#include <QMenu>

/*******************************************************************************
  Implementation of Model methods
  ******************************************************************************/

using namespace Eigen;
const static IOFormat CSVFormat(StreamPrecision, DontAlignCols, ", ", "\n");

Model::Model() : m_vertices(std::vector<Vertex3f>()),
                 m_triangles(std::vector<Triangle>()),
                 m_hasPartitions(false),
                 m_hasBounds(false),
                 m_hasAnchors(false),
                 m_data(),
                 m_count(0),
                 m_showAnchor(true)
{}

//------------------------------------------------------------------------------

Model::Model(string objFilename) : m_vertices(std::vector<Vertex3f>()),
                                   m_triangles(std::vector<Triangle>()),
                                   m_hasPartitions(false),
                                   m_hasBounds(false),
                                   m_hasAnchors(false),
                                   m_data(),
                                   m_count(0),
                                   m_showAnchor(true)
{
    loadOBJ(objFilename);
}

//------------------------------------------------------------------------------

Model::~Model()
{}

//------------------------------------------------------------------------------

void Model::loadOBJ(string objFilename)
{
    this->m_objFilename = objFilename;
    cout << "Model " << objFilename << " is Loading..." << endl;
    cout << __LINE__ << endl;
    ifstream file(objFilename, ifstream::in);
    string line;
    cout << __LINE__ << endl;
    if (file.is_open())
    {
        cout << __LINE__ << endl;
        int triNum = 0;
        while (getline(file, line))
        {
            // If line is empty go to the next one
            if (line.empty()) continue;
            // Split line in tokens
            vector<string> tokens = splitStr(line, ' ');
            if (tokens[0] == "v")
            {
                Vertex3f p;
                p.pos.x = stof(tokens[1]);
                p.pos.y = stof(tokens[2]);
                p.pos.z = stof(tokens[3]);
                m_vertices.push_back(p);
            }
            else if (tokens[0] == "vn")
            {
                Vec3f n;
                n.x = stof(tokens[1]);
                n.y = stof(tokens[2]);
                n.z = stof(tokens[3]);
                m_normals.push_back(n);
            }
            else if (tokens[0] == "f")
            {
                vector<string> tok1, tok2, tok3;
                tok1 = splitStr(tokens[1], '/');
                tok2 = splitStr(tokens[2], '/');
                tok3 = splitStr(tokens[3], '/');

                int v1 = stoi(tok1[0]) - 1;
                int v2 = stoi(tok2[0]) - 1;
                int v3 = stoi(tok3[0]) - 1;

                GLfloat col = GLfloat(0.8);
                m_triangles.push_back(Triangle(v1, v2, v3, stoi(tok1[2]) - 1,
                                               stoi(tok2[2]) - 1, stoi(tok3[2]) - 1,
                                               Color3(col, col, col)));

                // Push the adjacent vertices to the vertices
                //*
                m_vertices[v1].adjacentVerticies.push_back(v2);
                m_vertices[v1].adjacentVerticies.push_back(v3);
                m_vertices[v2].adjacentVerticies.push_back(v1);
                m_vertices[v2].adjacentVerticies.push_back(v3);
                m_vertices[v3].adjacentVerticies.push_back(v1);
                m_vertices[v3].adjacentVerticies.push_back(v2);

                // Push the adjacent triangles to the vertices
                m_vertices[v1].adjacentTriangles.push_back(triNum);
                m_vertices[v2].adjacentTriangles.push_back(triNum);
                m_vertices[v3].adjacentTriangles.push_back(triNum);
                triNum++;
            }
        }
        cout << __LINE__ << endl;
        file.close();
        cout << __LINE__ << endl;
        setUniqueAdjacentVertices();
        cout << "Model " << objFilename << " Loaded successfully!!" << endl;
    }
    else
    {
        cout << "Unable to open file: " << objFilename << endl;
        file.close();
    }
}

//------------------------------------------------------------------------------

void Model::loadOBJ()
{
    cout << "Model " << m_objFilename << " is Loading..." << endl;
    ifstream file(m_objFilename, ifstream::in);
    string line;
    if (file.is_open())
    {
        int triNum = 0;
        while (getline(file, line))
        {
            // If line is empty go to the next one
            if (line.empty()) continue;
            // Split line in tokens
            vector<string> tokens = splitStr(line, ' ');
            if (tokens[0] == "v")
            {
                Vertex3f p;
                p.pos.x = stof(tokens[1]);
                p.pos.y = stof(tokens[2]);
                p.pos.z = stof(tokens[3]);
                m_vertices.push_back(p);
            }
            else if (tokens[0] == "vn")
            {
                Vec3f n;
                n.x = stof(tokens[1]);
                n.y = stof(tokens[2]);
                n.z = stof(tokens[3]);
                m_normals.push_back(n);
            }
            else if (tokens[0] == "f")
            {
                vector<string> tok1, tok2, tok3;
                tok1 = splitStr(tokens[1], '/');
                tok2 = splitStr(tokens[2], '/');
                tok3 = splitStr(tokens[3], '/');

                int v1 = stoi(tok1[0]) - 1;
                int v2 = stoi(tok2[0]) - 1;
                int v3 = stoi(tok3[0]) - 1;

                GLfloat col = GLfloat(0.8);
                m_triangles.push_back(Triangle(v1, v2, v3, stoi(tok1[2]) - 1,
                    stoi(tok2[2]) - 1, stoi(tok3[2]) - 1,
                    Color3(col, col, col)));

                // Push the adjacent vertices to the vertices
                //*
                m_vertices[v1].adjacentVerticies.push_back(v2);
                m_vertices[v1].adjacentVerticies.push_back(v3);
                m_vertices[v2].adjacentVerticies.push_back(v1);
                m_vertices[v2].adjacentVerticies.push_back(v3);
                m_vertices[v3].adjacentVerticies.push_back(v1);
                m_vertices[v3].adjacentVerticies.push_back(v2);

                // Push the adjacent triangles to the vertices
                m_vertices[v1].adjacentTriangles.push_back(triNum);
                m_vertices[v2].adjacentTriangles.push_back(triNum);
                m_vertices[v3].adjacentTriangles.push_back(triNum);
                triNum++;
            }
        }
        file.close();
        setUniqueAdjacentVertices();
        cout << "Model " << m_objFilename << " Loaded successfully!!" << endl;
        //write graph file
        ofstream myfile;
        QString objpath = QString::fromStdString(m_objFilename);
        QRegExp rx("(\\.|\\/)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
        QStringList query = objpath.split(rx);
        QString name = query.value(query.length() - 2) + ".graph";
        myfile.open(name.toStdString());
        myfile << m_vertices.size() << " " << m_edges << endl;
        for (int i = 0; i < (int)m_vertices.size(); i++)
        {
            for (int j = 0; j < (int)m_vertices[i].adjacentVerticies.size(); j++)
            myfile << m_vertices[i].adjacentVerticies[j] + 1 << " ";
            myfile << endl;
        }
        myfile.close();
    }
    else
    {
        cout << "Unable to open file: " << m_objFilename << endl;
        file.close();
    }
}

//------------------------------------------------------------------------------

void Model::populateData()
{
    if (!m_data.empty()) m_data.clear();

    cout << "Data populating.." << endl;
    for (size_t i = 0; i < m_triangles.size(); i++)
    {
        m_data.push_back(m_vertices[m_triangles[i].v1].pos.x);
        m_data.push_back(m_vertices[m_triangles[i].v1].pos.y);
        m_data.push_back(m_vertices[m_triangles[i].v1].pos.z);
        m_data.push_back(m_normals[m_triangles[i].n1].x);
        m_data.push_back(m_normals[m_triangles[i].n1].y);
        m_data.push_back(m_normals[m_triangles[i].n1].z);
        //m_data.push_back(m_triangles[i].col.x);
        //m_data.push_back(m_triangles[i].col.y);
        //m_data.push_back(m_triangles[i].col.z);

        m_data.push_back(m_vertices[m_triangles[i].v2].pos.x);
        m_data.push_back(m_vertices[m_triangles[i].v2].pos.y);
        m_data.push_back(m_vertices[m_triangles[i].v2].pos.z);
        m_data.push_back(m_normals[m_triangles[i].n2].x);
        m_data.push_back(m_normals[m_triangles[i].n2].y);
        m_data.push_back(m_normals[m_triangles[i].n2].z);
        //m_data.push_back(m_triangles[i].col.x);
        //m_data.push_back(m_triangles[i].col.y);
        //m_data.push_back(m_triangles[i].col.z);

        m_data.push_back(m_vertices[m_triangles[i].v3].pos.x);
        m_data.push_back(m_vertices[m_triangles[i].v3].pos.y);
        m_data.push_back(m_vertices[m_triangles[i].v3].pos.z);
        m_data.push_back(m_normals[m_triangles[i].n3].x);
        m_data.push_back(m_normals[m_triangles[i].n3].y);
        m_data.push_back(m_normals[m_triangles[i].n3].z);
        //m_data.push_back(m_triangles[i].col.x);
        //m_data.push_back(m_triangles[i].col.y);
        //m_data.push_back(m_triangles[i].col.z);
    }
    m_count = m_data.count();
    cout << "Data populated sucessfully" << endl;
}

//------------------------------------------------------------------------------

void Model::populateColors()
{
    if (!m_colors.empty()) m_colors.clear();

    cout << "Colors populating.."  << m_colors.count() << endl;
    for (size_t i = 0; i < m_triangles.size(); i++)
    {
        m_colors.push_back(m_triangles[i].col.x);
        m_colors.push_back(m_triangles[i].col.y);
        m_colors.push_back(m_triangles[i].col.z);

        m_colors.push_back(m_triangles[i].col.x);
        m_colors.push_back(m_triangles[i].col.y);
        m_colors.push_back(m_triangles[i].col.z);

        m_colors.push_back(m_triangles[i].col.x);
        m_colors.push_back(m_triangles[i].col.y);
        m_colors.push_back(m_triangles[i].col.z);
    }
    m_countColors = m_colors.count();
    cout << "Colors populated sucessfully" << m_colors.count() << endl;
}

//------------------------------------------------------------------------------

void Model::populateDataWithError()
{
    if (!m_data.empty()) m_data.clear();

    if (m_vertices.size() != m_colorPerVertex.size())
    {
        std::cout << "Color And Vertices Size don't match!" << std::endl;
    }
    else
    {
        cout << "Error Data populating.." << endl;
        for (size_t i = 0; i < m_triangles.size(); i++)
        {
            m_data.push_back(m_vertices[m_triangles[i].v1].pos.x);
            m_data.push_back(m_vertices[m_triangles[i].v1].pos.y);
            m_data.push_back(m_vertices[m_triangles[i].v1].pos.z);
            m_data.push_back(m_normals[m_triangles[i].n1].x);
            m_data.push_back(m_normals[m_triangles[i].n1].y);
            m_data.push_back(m_normals[m_triangles[i].n1].z);
            m_data.push_back(m_colorPerVertex[m_triangles[i].v1].x);
            m_data.push_back(m_colorPerVertex[m_triangles[i].v1].y);
            m_data.push_back(m_colorPerVertex[m_triangles[i].v1].z);

            m_data.push_back(m_vertices[m_triangles[i].v2].pos.x);
            m_data.push_back(m_vertices[m_triangles[i].v2].pos.y);
            m_data.push_back(m_vertices[m_triangles[i].v2].pos.z);
            m_data.push_back(m_normals[m_triangles[i].n2].x);
            m_data.push_back(m_normals[m_triangles[i].n2].y);
            m_data.push_back(m_normals[m_triangles[i].n2].z);
            m_data.push_back(m_colorPerVertex[m_triangles[i].v2].x);
            m_data.push_back(m_colorPerVertex[m_triangles[i].v2].y);
            m_data.push_back(m_colorPerVertex[m_triangles[i].v2].z);

            m_data.push_back(m_vertices[m_triangles[i].v3].pos.x);
            m_data.push_back(m_vertices[m_triangles[i].v3].pos.y);
            m_data.push_back(m_vertices[m_triangles[i].v3].pos.z);
            m_data.push_back(m_normals[m_triangles[i].n3].x);
            m_data.push_back(m_normals[m_triangles[i].n3].y);
            m_data.push_back(m_normals[m_triangles[i].n3].z);
            m_data.push_back(m_colorPerVertex[m_triangles[i].v3].x);
            m_data.push_back(m_colorPerVertex[m_triangles[i].v3].y);
            m_data.push_back(m_colorPerVertex[m_triangles[i].v3].z);
        }
        m_count = m_data.count();
        cout << "Data populated sucessfully" << endl;
    }
}

//------------------------------------------------------------------------------

void Model::loadPartitions(string partsFilename)
{
      cout << "Patitions Loading..." << partsFilename << endl;

      ifstream file(partsFilename, ifstream::in);

      int num, max = 0;

      if (file.is_open())
      {
          m_partsFilename = partsFilename;
            int i = 0;
            while (file >> num)
            {
                  m_vertices[i++].part = num;
                  if (max < num) max = num;
            }
      }
      else cout << "File " << partsFilename << " not found" << endl;
      file.close();

      default_random_engine generator;
      gamma_distribution<GLfloat> distribution(10, 10);

      for (int i = 0; i <= max; i++)
      {
            Color3 col(fmod(distribution(generator) / 100., 1.0),
            fmod(distribution(generator) / 100., 1.0),
            fmod(distribution(generator) / 100., 1.0));
            m_palette.push_back(col);
      }
      setColorsToPartitions();

      cout << "Patitions Loaded Succesfully" << partsFilename << endl;
}

//------------------------------------------------------------------------------

void Model::loadPartitions()
{
    cout << "Patitions Loading..." << m_partsFilename << endl;

    ifstream file(m_partsFilename, ifstream::in);

    int num, max = 0;

    if (file.is_open())
    {
        int i = 0;
        while (file >> num)
        {
            m_vertices[i++].part = num;
            if (max < num) max = num;
        }
    }
    else cout << "File " << m_partsFilename << " not found" << endl;
    file.close();

    default_random_engine generator;
    gamma_distribution<GLfloat> distribution(10, 10);

    for (int i = 0; i <= max; i++)
    {
        Color3 col(fmod(distribution(generator) / 100., 1.0),
                   fmod(distribution(generator) / 100., 1.0),
                   fmod(distribution(generator) / 100., 1.0));
        m_palette.push_back(col);
    }

    setColorsToPartitions();

    cout << "Patitions Loaded Succesfully" << m_partsFilename << endl;
}

//------------------------------------------------------------------------------

void Model::getVerticesFromPartition(vector<Vertex3f> &Vpart, vector<int> &Idx, int partition)
{
    int k = 0;

    for (int j = 0; j < (int)m_vertices.size(); j++)
    {
        if (m_vertices[j].part == partition)
        {
            Vpart.push_back(m_vertices[j]);
            Idx.push_back(j);
            k++;
        }
    }
}

//------------------------------------------------------------------------------

void Model::evaluateDeltaCoordinates(int nparts)
{
    ofstream testfile;
    testfile.open("TestDeltaMatlab.txt");

    for (int j = 0; j < (int)m_vertices.size(); j++)
    {
        int tmp = 0;
        Vec3f sum = Vec3f();
        Vertex3f& curVec = m_vertices[j];

        for (int k = 0; k < (int)curVec.adjacentVerticies.size(); k++)
        {
            if (m_vertices[curVec.adjacentVerticies[k]].part == curVec.part)
            {
                sum += m_vertices[curVec.adjacentVerticies[k]].pos;
                tmp++;
            }
        }
        sum /= (GLfloat)tmp;
        curVec.delta = curVec.pos - sum;
        //cout<<" Sum: "<<sum<<" Tmp: "<<tmp << endl;
        testfile << curVec.delta.x << " " << curVec.delta.y << " " << curVec.delta.z << endl;
    }
    testfile.close();
    cout << "The file was written succesfully";
}

//------------------------------------------------------------------------------

void Model::surfaceReconstruction_new(double CR)
{
    Matrix<double, Dynamic, Dynamic> A, I, L, Lx, E;
    Matrix<double, Dynamic, 3> Delta, Deltax, xsol, x0, Derr, D0;
    double VisErr = 0.0;

    //ofstream myfile ("Test.txt");

    //Recostruct each Submesh separetly
    for (int i = 0; i < m_indices.size(); i++)
    {
        int row = m_indices[i].size();
        //Evaluate Lamda matrix for submesh
    

        std::cout << "[Construction of Laplacian Matrix:] Start " << endl;

        MatrixXd::Zero(row, row);
        MatrixXd::Identity(row, row);
        VectorXd d(row);

        d.setZero(row);
        xsol.setZero(row, 3);
        x0.setZero(row, 3);

        L.setZero(row, row);
        A.setZero(row, row);
        I.setIdentity(row, row);
        Delta.setZero(row, 3);
        E.setZero(row, row);

        std::vector<int>::iterator it;
        int check;

        for (int j = 0; j < m_indices[i].size(); j++)
        {
            //construct Adjacency matrix
            //Check if adjacent vertices belong to the same partition
            for (int k = 0; k < m_vertices[m_indices[i][j]].adjacentVerticies.size(); k++)
            {
                it = find(m_indices[i].begin(), m_indices[i].end(), m_vertices[m_indices[i][j]].adjacentVerticies[k]);
                if (it != m_indices[i].end())
                {
                    check = (int)* it;
                    check = it - m_indices[i].begin();
                    //cout<<it - Indices[i].begin()<<endl;
                    A(j, check) = 1; A(check, j) = 1;
                }
            }
        }

        //// Evaluate Laplacian per Submesh
        d.transpose() = (A.rowwise().sum());

        d = d.array().inverse();
        A = d.asDiagonal()*A;
        L = I - A;

        // find matrix with EigenValues
        //JacobiSVD<MatrixXd> svd(L, ComputeThinU | ComputeThinV);
        //E=svd.matrixV();
        std::cout<<"Calculating EigenValues of the Laplacian Operator...."<<endl; //Too slow operation
        EigenSolver<MatrixXd> es(L);
        E = es.eigenvectors().real();
    

        std::cout << "[Construction of Laplacian Matrix:] Done "<< std::endl;

        //Perform Compress and Decompress per submesh with COnventional Approach
        classicalDecode(CR, i, E, 1);//if 1 use DCT otherwise use the eigenvectors of the Laplacian for each submesh
        classicalDecode(CR, i, E, 0);// Very Slow Dictionary but provides much better results

        //Perform Compress and Decompress with MBL approach
        mblDecode(CR,i,L,E);
    
    
        for (int j = 0; j < m_indices[i].size(); j++)
        {
            xsol(j, 0) = m_vertices[m_indices[i][j]].rpos.x;
            xsol(j, 1) = m_vertices[m_indices[i][j]].rpos.y;
            xsol(j, 2) = m_vertices[m_indices[i][j]].rpos.z;

            x0(j, 0) = m_vertices[m_indices[i][j]].pos.x ;
            x0(j, 1) = m_vertices[m_indices[i][j]].pos.y;
            x0(j, 2) = m_vertices[m_indices[i][j]].pos.z;
        }

        // Evaluate Visual Error
        xsol.col(0) -= x0.col(0); xsol.col(1) -= x0.col(1); xsol.col(2) -= x0.col(2);
    
        double NMSVE = xsol.squaredNorm()/x0.squaredNorm();

        std::cout << "[Surface Reconstruction:] Done "<< std::endl;

        std::cout << "Submesh: " << i << " Reconstructed" << endl;
        //std::cout << "with Euclidean NMSE: " << 10 * log10(NMSVE) << " dB" << endl;

        //std::cout<<"Delta Error:"<< 10*log10(pow(VDe.norm()/VD0.norm(),2))<<" dB"<<endl;
        VisErr += NMSVE;
    }
    VisErr /= m_indices.size();
}

//------------------------------------------------------------------------------

void Model::compressDelta(double CR, int nparts)
{
    std::default_random_engine generator;

    double param, fractpart, intpart;
    //vector<vector<Vertex3f>> Vpart;

    param = (double)m_vertices.size()*CR;
    fractpart = modf(param, &intpart);

    cout << "Number of Original Samples:" << m_vertices.size() << endl;
    cout << "Generate " << (int)intpart << " Encoding Samples" << endl;

    for (int k = 0; k < nparts; k++)
    {
        vector<Vertex3f> vtmp;
        vector<int> idxtmp;
        getVerticesFromPartition(vtmp, idxtmp, k);
        m_indices.push_back(idxtmp);

        //Vpart.push_back(vtmp);
        vector<Vec3f> enc_tmp;
        Vec3f sum = Vec3f();
        param = (double)vtmp.size()*CR;
        fractpart = modf(param, &intpart);

        for (int i = 0; i < intpart; i++)
        {
            for (int j = 0; j < vtmp.size(); j++)
            {
                std::uniform_int_distribution<int> distribution(0, 1);
                int rnd = (distribution(generator) > 0) ? 1 : -1;
                vtmp[j].delta *= (GLfloat)rnd;
                sum += vtmp[j].delta;
            }
            enc_tmp.push_back(sum);
        }
        m_encDelta.push_back(enc_tmp);

        cout << "The size of partition " << k << " is " << vtmp.size() << endl;
        cout << "The size of the encoding vector " << k << " is " << m_encDelta[k].size() << endl;
    }
}

// Perform Classical Decode per Submesh.....
void Model::classicalDecode(double CR, int npart, Eigen::MatrixXd E, int Dict)
{
    double alpha = CR;			// Ratio of the cs-measurements.
    int n = m_indices[npart].size();					// Size of the original signal x0
    double fractpart, intpart;
    fractpart = modf (alpha*((double) n) , &intpart);
   
    int m = (int) intpart;
  

    std::cout << "[Conventional Compression/Decompression] Start: " << npart << std::endl;
  
    // Start Encoding.......

    Eigen::MatrixXd dctmtx(n, n);
    //Eigen::MatrixXd E(n, n);
    Eigen::VectorXd tx(n), ty(n), tz(n), tx0(n), ty0(n), tz0(n);

    double sqn = 1.0 / sqrt((double)n);
    double sq2n = sqrt(2.0 / ((double)n));
    const double PI = 3.141592653589793;

    for (int j = 0; j < m_indices[npart].size(); j++)
    {
        tx0[j] = m_vertices[m_indices[npart][j]].pos.x;
        ty0[j] = m_vertices[m_indices[npart][j]].pos.y;
        tz0[j] = m_vertices[m_indices[npart][j]].pos.z;
    }

    if(Dict==1)
    {
    std::cout<<"Selected Dictionary: DCT"<<endl;
    //Construct the DCT Matrix
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (i == 0)
            dctmtx(i, j) = sqn;
            else if (i>0)
            dctmtx(i, j) = sq2n*cos((PI*(2 * j + 1)*i) / ((double)2 * n));
        }
    }

    tx = dctmtx*tx0; ty = dctmtx*ty0; tz = dctmtx*tz0;}

    else
    {//Much more efficient Dictinary
        std::cout<<"Selected Dictionary: GFT"<<endl;
     
        tx = E.inverse()*tx0;ty = E.inverse()*ty0;tz = E.inverse()*tz0;
    }

    //ofstream myfile ("TestMATLAB_Lap.csv");
    //myfile<<L.format(CSVFormat)<<endl;
    //myfile<<endl;
    //myfile.close();

    //find the n-m lower values to 0
    Eigen::VectorXd xsort(n),ysort(n),zsort(n);
    xsort<<tx.array().abs();
    ysort<<ty.array().abs();
    zsort<<tz.array().abs();
    std::sort(xsort.data(),xsort.data()+xsort.size());
    std::sort(ysort.data(),ysort.data()+ysort.size());
    std::sort(zsort.data(),zsort.data()+zsort.size());

    int ZeroCoefs = 0;
    //Perform Hard Thresholding
    for (int j = 0; j < n; j++)
    {
        if (abs(tx[j]) < xsort[n - m]) { tx[j] = 0.0; ZeroCoefs++; }
        if (abs(ty[j]) < ysort[n - m]) { ty[j] = 0.0; ZeroCoefs++; }
        if (abs(tz[j]) < zsort[n - m]) { tz[j] = 0.0; ZeroCoefs++; }
    }
  
    ofstream myfile ("TestMATLAB_CLASC.csv");
    myfile<<tx<<endl;
    myfile<<endl;
    myfile.close();

    //MatrixXd C(tx.rows(), tx.cols()+tx0.cols());
    //C << tx, tx0;
    // cout<< intpart<<endl;
    //ofstream myfile2 ("TestMATLAB.csv");
    // myfile2<<C<<endl;
    // myfile2<<endl;
    // myfile2.close();

    // Start Decoding.......

    double CRcalc = (double)ZeroCoefs / ((double)3 * n);
  
    if(Dict==1)
    {
        tx = dctmtx.inverse()*tx;
        ty = dctmtx.inverse()*ty;
        tz = dctmtx.inverse()*tz;
    }
    else
    {
        tx = E*tx;
        ty = E*ty;
        tz = E*tz;
    }
    
    //copy back to delta
    for (int j = 0; j < m_indices[npart].size(); j++)
    {
        m_vertices[m_indices[npart][j]].rpos.x = tx[j];
        m_vertices[m_indices[npart][j]].rpos.y = ty[j];
        m_vertices[m_indices[npart][j]].rpos.z = tz[j];
    }
    tx-=tx0;ty-=ty0;tz-=tz0;
  
    double enumer = tx.squaredNorm()+ty.squaredNorm()+tz.squaredNorm();
    double denumer = tx0.squaredNorm()+ty0.squaredNorm()+tz0.squaredNorm();

    std::cout << "[Conventional Compression/Decompression] Done "<< std::endl;
    std::cout << "Compression Ratio: " << CRcalc << std::endl;

    std::cout << "Recosntruction Error:" <<10*log10(enumer/denumer)<<" dB"<<std::endl;
}

//------------------------------------------------------------------------------

void Model::mblDecode(double CR, int npart, Eigen::MatrixXd L, Eigen::MatrixXd E)
{
    double alpha = CR;              // Ratio of the cs-measurements.
    int n = m_indices[npart].size(); // Size of the original signal x0
    double fractpart, intpart;
    fractpart = modf (alpha*((double) n) , &intpart);
   
    int m = (int) intpart;
  

    std::cout << "[MBL Compression/Decompression] Start: " << npart << std::endl;
  
    // Start Encoding.......
    Eigen::MatrixXd A= MatrixXd::Random(m,n);
    Eigen::MatrixXd Ax(m+n,n);
    Eigen::VectorXd Enx(m), Eny(m), Enz(m), tx0(n), ty0(n), tz0(n);
    Eigen::VectorXd Enx_ex(m+n), Eny_ex(m+n), Enz_ex(m+n);
    Eigen::VectorXd v0=VectorXd::Zero(n);
  
    // Read Values for the given submesh
    for (int j = 0; j < m_indices[npart].size(); j++)
    {
        tx0[j] = m_vertices[m_indices[npart][j]].pos.x;
        ty0[j] = m_vertices[m_indices[npart][j]].pos.y;
        tz0[j] = m_vertices[m_indices[npart][j]].pos.z;
    }

    Enx = A*tx0; Eny = A*ty0; Enz = A*tz0;

    // Extend coding matrix with Laplacian values 
    // and coded vector with zeros respectively... (see Sec. Exploiting the local Smoothness)
 
    Ax.topRows(m) = A*E;
    Ax.bottomRows(n) = L*E;

    Enx_ex.topRows(m) = Enx;
    Enx_ex.bottomRows(n)= v0; 
    Eny_ex.topRows(m) = Eny;
    Eny_ex.bottomRows(n)= v0; 
    Enz_ex.topRows(m) = Enz;
    Enz_ex.bottomRows(n)= v0; 

    //Start MBLDecoding.....
    //Test Dictionary in MATLAB
    //ofstream myfile ("TestMATLAB.csv");
    //myfile<<E.inverse()*tx0<<endl;
    //myfile<<endl;
    //myfile.close();

    // Select left columns
    Eigen::MatrixXd Anew = Ax.leftCols(m);

    //Construct a Toeplitz Symmetric Matrix that models Correlation....
    Eigen::MatrixXd C0(m,m), Cx(m,m);
    Eigen::MatrixXd I= MatrixXd::Identity(n+m,n+m);
    Eigen::MatrixXd temp(n+m,n+m);
    Eigen::VectorXd xest(m), yest(m), zest(m);
    Eigen::VectorXd xp = VectorXd::Zero(n);
    Eigen::VectorXd yp = VectorXd::Zero(n);
    Eigen::VectorXd zp = VectorXd::Zero(n);

    double r = 0.7;

    for(int i=0; i<m; i++)
    {
        for(int j=0; j<m; j++){C0(i,j) = pow(r,abs(i-j));} 
    }

    double lambda = 0.0001;
    int Iterations=1;
    for(int it = 0; it<Iterations; it++)
    {
        temp = lambda*I+Anew*C0*Anew.transpose();
        temp = temp.inverse();
        // Mean value
        xest = C0*Anew.transpose()*temp*Enx_ex;
        yest = C0*Anew.transpose()*temp*Eny_ex;
        zest = C0*Anew.transpose()*temp*Enz_ex;
      
        //Covariance estimation
        Cx = C0-C0*Anew.transpose()*temp*Anew*C0;
    }

    xp.topRows(m) = xest;
    yp.topRows(m) = yest;
    zp.topRows(m) = zest;

    //Test Dictionary in MATLAB
    ofstream myfile ("TestMATLAB_MBL.csv");
    myfile<<xp<<endl;
    myfile<<endl;
    myfile.close();
 

    xp = E*xp; yp = E*yp; zp = E*zp;
    
    ofstream myfile2 ("TestMATLAB_Eig.csv");
    myfile2<<E.format(CSVFormat)<<endl;
    myfile2<<endl;
    myfile2.close();

    //int pause;
    //cin>> pause;

    // Write Reconstructed values
    for (int j = 0; j < m_indices[npart].size(); j++)
    {
        m_vertices[m_indices[npart][j]].rpos.x = xp(j);
        m_vertices[m_indices[npart][j]].rpos.y = yp(j);
        m_vertices[m_indices[npart][j]].rpos.z = zp(j);
    }

    xp-=tx0;yp-=ty0;zp-=tz0;
  
    double enumer = xp.squaredNorm()+yp.squaredNorm()+zp.squaredNorm();
    double denumer = tx0.squaredNorm()+ty0.squaredNorm()+tz0.squaredNorm();

    std::cout << "[MBL Compression/Decompression] Done "<< std::endl;

    std::cout << "Recosntruction Error:" <<10*log10(enumer/denumer)<<" dB"<<std::endl;
}

//------------------------------------------------------------------------------

void Model::setColorsToPartitions()
{
    for (size_t i = 0; i < m_triangles.size(); i++)
    {
        if ((m_vertices[m_triangles[i].v1].part == m_vertices[m_triangles[i].v2].part) &&
            (m_vertices[m_triangles[i].v2].part == m_vertices[m_triangles[i].v3].part))
        {
            m_triangles[i].col = m_palette[m_vertices[m_triangles[i].v1].part];
        }
        else
            m_triangles[i].col = Color3(1.0, 1.0, 1.0);
    }
}

//------------------------------------------------------------------------------

void Model::loadBounds(string boundsFilename)
{
    cout << "Bounds Loading..." << boundsFilename << endl;

    ifstream file(boundsFilename, ifstream::in);

    if (file.is_open())
    {
        m_boundsFilename = boundsFilename;
        int num1, num2, num3;
        while ((file >> num1) && (file >> num2) && (file >> num3))
        {
            for (int i = 0; i < (int)m_triangles.size(); i++)
            {
                if ((m_triangles[i].v1 == num1) && m_triangles[i].v2 == num2 && m_triangles[i].v3 == num3)
                {
                    m_triangles[i].col = Color3(1, 0, 0);
                    break;
                }
            }
        }
    }
    file.close();
}

//------------------------------------------------------------------------------

void Model::loadBounds()
{
    cout << "Bounds Loading..." << m_boundsFilename << endl;

    ifstream file(m_boundsFilename, ifstream::in);

    int count = 0;
    if (file.is_open())
    {
        int num1, num2, num3;
        while ((file >> num1) && (file >> num2) && (file >> num3))
        {
            for (int i = 0; i < (int)m_triangles.size(); i++)
            {
                if ((m_triangles[i].v1 == num1) && m_triangles[i].v2 == num2 && m_triangles[i].v3 == num3)
                {
                    m_triangles[i].col = Color3(1, 0, 0);
                    count++;
                    break;
                }
            }
        }
    }
    cout << "Total " << count << endl;
    file.close();
}

//------------------------------------------------------------------------------

void Model::setUniqueAdjacentVertices()
{
    for (int i = 0; i < (int)m_vertices.size(); i++)
    {
        vector<int>& currVec = m_vertices[i].adjacentVerticies;
        vector<int>::iterator r, w;
        set< int > tmpset;
        for (r = currVec.begin(), w = currVec.begin(); r != currVec.end(); ++r)
        {
            if (tmpset.insert(*r).second)
            {
                *w++ = *r;
            }
        }
        currVec.erase(w, currVec.end());
    }

    m_edges = 0;
    for (int i = 0; i < (int)m_vertices.size(); i++)
    {
        m_edges += (int)m_vertices[i].adjacentVerticies.size();
    }
    m_edges /= 2;
}

//------------------------------------------------------------------------------

void Model::loadAnchorPoints(string anchorsFilename)
{
    m_anchorsFilename = anchorsFilename;
    ifstream file(anchorsFilename);
    float num;
    while (file >> num) m_anchorPoints.push_back((int)num);

    file.close();
}

//------------------------------------------------------------------------------

void Model::loadAnchorPoints()
{
    ifstream file(m_anchorsFilename);
    float num;
    while (file >> num) m_anchorPoints.push_back((int)num);

    file.close();
}

//------------------------------------------------------------------------------

void Model::draw()
{
    /*
    glBegin(GL_TRIANGLES);
    for (size_t i = 0; i < triangles.size(); i++)
    {
        glColor3fv(triangles[i].col.p);

        glNormal3fv(normals[triangles[i].n1].p);
        glVertex3fv(vertices[triangles[i].v1].pos.p);
        glNormal3fv(normals[triangles[i].n2].p);
        glVertex3fv(vertices[triangles[i].v2].pos.p);
        glNormal3fv(normals[triangles[i].n3].p);
        glVertex3fv(vertices[triangles[i].v3].pos.p);
        }
        glEnd();
        if (showAnchor)
        {
            glColor3f(1.0, 0.0, 0.0);
            GLfloat *center;
            for (size_t i = 0; i < anchorPoints.size(); i++)
            {
            center = vertices[anchorPoints[i]].pos.p;
            glPushMatrix();
            glTranslatef(center[0], center[1], center[2]);
            //glutSolidSphere(0.5, 9, 9);
            glPopMatrix();
        }
    }
    //*/
}

//------------------------------------------------------------------------------

void Model::saveOBJ(string objFilename)
{
    cout << "Exporting OBJ..." << endl;

    ofstream file(objFilename);
    file << "# Blender v2.71 (sub 0) OBJ File: " << objFilename << endl;
    file << "# www.blender.org" << endl;
    file << objFilename << endl;
    for (int i = 0; i < (int)m_vertices.size(); i++)
    {
        file << "v" << " " << m_vertices[i].pos.x
             << " " << m_vertices[i].pos.y
             << " " << m_vertices[i].pos.z << endl;
    }
    for (int i = 0; i < (int)m_normals.size(); i++)
    {
        file << "vn" << " " << m_normals[i].x
             << " " << m_normals[i].y
             << " " << m_normals[i].z << endl;
    }
    file << "s 1" << endl;
    for (int i = 0; i < (int)m_triangles.size(); i++)
    {
        file << "f" << " " << m_triangles[i].v1 + 1 << "//" << m_triangles[i].n1 + 1
             << " " << m_triangles[i].v2 + 1 << "//" << m_triangles[i].n2 + 1
             << " " << m_triangles[i].v3 + 1 << "//" << m_triangles[i].n3 + 1 << endl;
    }
    file.close();

    cout << "OBJ Export Complete" << endl;
}

//------------------------------------------------------------------------------

void Model::saveBounds(string boundsFilename)
{
    cout << "Exporting Bounds..." << endl;

    ofstream file(boundsFilename);
    for (size_t i = 0; i < m_triangles.size(); i++)
    {
        Color3 black;
        if (m_triangles[i].col == black)
            file << m_triangles[i].v1 << " " << m_triangles[i].v2 << " " << m_triangles[i].v3 << endl;
    }
    file.close();

    cout << "Bounds Export Complete" << endl;
}

//------------------------------------------------------------------------------

void Model::saveGraph(string graphFilename)
{
    cout << "Exporting graph..." << endl;

    ofstream file(graphFilename);

    file << m_vertices.size() << " " << m_edges << endl;
    for (int i = 0; i < (int)m_vertices.size(); i++)
    {
        for (int j = 0; j < (int)m_vertices[i].adjacentVerticies.size(); j++)
        {
            file << m_vertices[i].adjacentVerticies[j] + 1 << " ";
        }
        file << endl;
    }
    file.close();

    cout << "Graph Export Complete" << endl;
}

//------------------------------------------------------------------------------

const GLfloat* Model::constData() const
{
    return m_data.constData();
}

//------------------------------------------------------------------------------

const GLfloat* Model::constColors() const
{
    return m_colors.constData();
}

//------------------------------------------------------------------------------

int Model::count() const
{
    return m_count;
}

//------------------------------------------------------------------------------

int Model::countColors() const
{
    return m_countColors;
}

//------------------------------------------------------------------------------

int Model::vertexCount() const
{
    return m_count / 6;
}

//------------------------------------------------------------------------------

vector<string>& Model::splitStr(const string &s, char delim, vector<string> &elems)
{
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim))
    {
        elems.push_back(item);
    }
    return elems;
}

//------------------------------------------------------------------------------

vector<string> Model::splitStr(const std::string &s, char delim)
{
    vector<string> elems;
    splitStr(s, delim, elems);
    return elems;
}

//------------------------------------------------------------------------------

ostream& operator << (ostream& os, const Model& m)
{
    for (size_t i = 0; i < m.m_triangles.size(); i++)
    {
        os << m.m_vertices[m.m_triangles[i].v1].pos << " " << m.m_normals[m.m_triangles[i].n1] << endl;
        os << m.m_vertices[m.m_triangles[i].v2].pos << " " << m.m_normals[m.m_triangles[i].n2] << endl;
        os << m.m_vertices[m.m_triangles[i].v3].pos << " " << m.m_normals[m.m_triangles[i].n3] << endl;
    }
    return os;
}