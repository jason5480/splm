#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QMatrix4x4>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QInputEvent>
#include <string>
#include "model.h"

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram)

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
Q_OBJECT

public:
    GLWidget(QWidget *parent = 0);
    GLWidget(std::string filename, QWidget *parent = 0);
    ~GLWidget();
    
    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void setModelName(QString filename);
    void setModelParts(QString filename);
    void setModelBounds(QString filename);
    void init(bool withColorPerVertex = false);
    Model* getModel(){ return &m_model; }
    void setModel(const Model& model_) { m_model = Model(model_); }
    void colorsChanged();

private:
    void setupVertexAttribs();

public:
    int m_wireframe;
    bool m_partitions;
    bool m_anchors;

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;
    void resizeGL(int width, int height) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
    int  mkModif(QInputEvent *event);
     
private:
    int m_xRot;
    int m_yRot;
    int m_zRot;
    int m_dx;
    int m_dy;
    int m_dz;
    float m_cDist;
    float m_zoomStep;
    QPoint m_lastPos;
    Model m_model;
    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_modelVbo;
    QOpenGLBuffer m_colorVbo;
    QOpenGLShaderProgram *m_program;
    int m_projMatrixLoc;
    int m_mvMatrixLoc;
    int m_viewMatrixLoc;
    int m_modelMatrixLoc;
    int m_normalMatrixLoc;
    int m_lightPosCamLoc;
    int m_lightPosWorLoc;
    int m_lightColLoc;
    int m_lightPowLoc;
    int m_isBlackLoc;
    QMatrix4x4 m_proj;
    QMatrix4x4 m_camera;
    QMatrix4x4 m_world;
    bool m_transparent;
    
public slots:
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);
    void cleanup();

signals:
    void wireframeChanged(Qt::CheckState state);
};

#endif
