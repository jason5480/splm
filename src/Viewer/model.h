#ifndef _Model_H_
#define _Model_H_

//--------------------------------------------------------------------------------//
//                        VVR LAB 2015                                            //
//   Sparse Processing of Laplacian meshes for efficient Geometry Compression     //
//Authors: Aris S. Lalos, Iason Nikolas, Viktor Kyriazakos, Konstantinos Moustakas//
//code written by A. Lalos, I. Nikolas, V. Kyriazakos                             //
//--------------------------------------------------------------------------------//

#include "triangle.h"
#include "vertex.h"
#include "qopengl.h"
#include <QVector>
#include <string>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/Cholesky>

typedef Vec3<GLfloat>   Vec3f;
typedef Vertex<GLfloat> Vertex3f;

using namespace std;

/*******************************************************************************
  Model
  ******************************************************************************/

class Model
{
public:
    Model();
    //Model(Model& other);
    Model(string objFilename);
    ~Model();

    // Methods
    void loadOBJ(string objFilename);
    void loadOBJ();
    void loadPartitions(string partsFilename);
    void loadPartitions();
    void loadBounds(string boundsFilename);
    void loadBounds();
    void setError(vector<double> error){ this->m_err = error; }
    void setErrorMax(double max){ this->m_maxErr = max; }
    void setErrorMin(double min){ this->m_minErr = min; }
    void getVerticesFromPartition(vector<Vertex3f> &SubVertices, vector<int> &Idx, int partition);
    void loadAnchorPoints(string anchorsFilename);
    void loadAnchorPoints();
    void evaluateDeltaCoordinates(int nparts);
    void surfaceReconstruction_new(double CR);
    void compressDelta(double CR, int nparts); // only for experimental purposes
    void classicalDecode(double CR, int npart, Eigen::MatrixXd E, int Dict);
    void mblDecode(double CR, int npart, Eigen::MatrixXd L, Eigen::MatrixXd E);
    void saveOBJ(string objFilename);
    void saveBounds(string boundsFilename);
    void saveGraph(string graphFilename);
    void populateData();
    void populateColors();
    void populateDataWithError();
    void draw();

    const GLfloat *constData() const;
    const GLfloat *constColors() const;
    int count() const;
    int countColors() const;
    int vertexCount() const;

public:
    // Data members
    string m_objFilename;
    string m_partsFilename;
    string m_anchorsFilename;
    string m_boundsFilename;
    //string graphFilename;
    vector<Vertex3f> m_vertices;
    vector<vector<int>> m_indices;
    vector<double> m_err;
    vector<vector<Vec3f>> m_encDelta;
    vector<vector<Vec3f>> m_encPos;
    vector<Vec3f> m_normals;
    vector<Triangle> m_triangles;
    vector<int> m_anchorPoints;
    vector<Color3> m_palette;
    vector<Color3> m_colorPerVertex;

    bool m_showAnchor;
    int m_edges;
    double m_visualErr;
    double m_maxErr;
    double m_minErr;

    friend ostream& operator << (ostream& os, const Model& m);
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

private:
    // Methods
    void setColorsToPartitions();
    void setUniqueAdjacentVertices();
    vector<string>& splitStr(const string &s, char delim, vector<string> &elems);
    vector<string>  splitStr(const std::string &s, char delim);

private:
    // Data members
    bool m_hasPartitions;
    bool m_hasBounds;
    bool m_hasAnchors;
    QVector<GLfloat> m_data;
    QVector<GLfloat> m_colors;
    int m_count;
    int m_countColors;
};

#endif
