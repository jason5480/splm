#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "ui_mainwindow.h"

class MainWindow : public QMainWindow, private Ui::MainWindow
{
Q_OBJECT

public:
    MainWindow();

    void generateLaplacianMesh();
    
    QTimer m_timer;
    bool animate;
    float angleY;

protected:
    void keyPressEvent(QKeyEvent *) Q_DECL_OVERRIDE;

private slots:
    void on_actionAddNew_triggered();
    void on_actionPartitions_triggered();
    void on_actionBounds_triggered();
    void on_actionAnchorPoints_triggered();
    void on_actionShowError_triggered();
    void on_actionSaveAs_triggered();
    void on_actionGraph_triggered();
    void on_actionMeTiS_triggered();
    void on_actionEncoder_triggered();
    void on_actionAnimate_triggered();
    void changeAngle();

signals:
    void partitionsLoaded(bool state);
    void boundsLoaded(bool state);
    void anchorsLoaded(bool state);
    void errorLoaded(bool state);
    void angleChanged(float angle);
};

#endif
