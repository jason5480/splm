# Sparse Coding of Dense 3d Meshes in Mobile Cloud Applications #

### Compatible Platforms ###
* Windows (Visual Studio 2010/2013)
* Linux

### Dependencies ###

* [Qt 5](http://www.qt.io/download-open-source/)
* [Eigen] (https://bitbucket.org/eigen/eigen/)
* [OpenGL](https://www.opengl.org/)
Don't forget to add QTDIR, EIGEN_ROOT and GKLIB_PATH to system variables.
GKLIB is the src folder.

### Who do I talk to? ###

* [Simulation: Aris Lalos](mailto:aris.lalos@ece.upatras.gr)
* [Visualization: Iason Nikolas](mailto:iason.nikolas@ece.upatras.gr)